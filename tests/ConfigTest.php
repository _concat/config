<?php

namespace Concat\Config\Tests;

use Concat\Config\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->config = new Config(__DIR__."/fixtures/config.php");
    }

    public function testDump()
    {
        $this->assertEquals([
            "paths.root"        => "/",
            "assets.cache"      => true,
            "assets.compress"   => false,
        ], $this->config->dump());
    }

    public function testGet()
    {
        $this->assertEquals("/", $this->config->get("paths.root"));
    }

    public function testGetDefault()
    {
        $this->assertEquals(".", $this->config->get("paths.cache", "."));
    }

    public function testGetArrayAccess()
    {
        $this->assertEquals("/", $this->config["paths.root"]);
    }
}
