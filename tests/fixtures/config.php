<?php

return [

    "paths" => require ("paths.php"),

    "assets" => [
        "cache" => true,
        "compress" => false,
    ]
];
