<?php

namespace Concat\Config;

use function \Concat\Helpers\Arrays\array_dot;

class Config implements \ArrayAccess
{
    public function __construct($data, $defaults = [])
    {
        if (is_array($data)) {
            //
            $this->load($data);
        } elseif (is_readable($data)) {
            $config = require $data;

            // replace defaults
            $config = array_replace_recursive($defaults, $config);

            if (is_array($config)) {
                $this->load($config);
            } else {
                throw new \Exception("Evaluation did not result in an array");
            }
        } else {
            throw new \Exception("Path is not readable: $data");
        }
    }

    // load php
    public function load(array $array)
    {
        // for dot notation access via 'get'
        $this->config = array_dot($array);
    }

    public function get($key, $default = null)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }

        if (func_num_args() === 1) {
            // no default provided
            throw new \Exception("No config found for $key");
        }

        return $default;
    }

    public function dump()
    {
        return $this->config;
    }

    public function offsetExists($key)
    {
        return isset($this->config[$key]);
    }

    public function offsetGet($key)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        throw new \Exception("No config found for $key");
    }

    public function offsetSet($key, $value)
    {
        throw new \Exception("Can not override config data");
    }

    public function offsetUnset($key)
    {
        throw new \Exception("Can not unset config data");
    }
}
